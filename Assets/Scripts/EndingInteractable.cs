using UnityEngine;
using UnityEngine.SceneManagement;

public class EndingInteractable : Interactable {

    public override void OnInteract(Interactor interactor) {
        SceneManager.LoadScene("End");
    }
}
