using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardSwipeTaskInteractable : CanvasInteractable
{
    public override void OnInteract(Interactor interactor)
    {

        Engage(interactor);
        var stm = _taskCanvas.transform.GetComponent<SwipeTaskManager>();

        stm.OnFail += (_, __) =>
        {
            Debug.Log("Failed");
        };

        stm.OnSuccess += (_, __) =>
        {
            Debug.Log("Success");
            StartCoroutine(Utils.DelayedCall(() => Cleanup(interactor)));
        };

        _taskCanvas.SetActive(true);
    }
}
