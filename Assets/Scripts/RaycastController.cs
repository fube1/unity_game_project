using System;
using UnityEngine;

public class RaycastController : MonoBehaviour {
    public event EventHandler OnSee;

    [Header("Audio")]
    public AudioSource audioSource;

    public GameObject player;
    public GameObject sussy;
    public Camera cam;


    //private void FixedUpdate() {
    //    var pos = transform.position;
    //    pos.y += 20f;
    //    int mask = 0 << 3 & 2;
    //    Debug.DrawRay(pos,transform.forward);
    //    if (Physics.SphereCast(pos, 2f, transform.forward, out RaycastHit hit, Mathf.Infinity, ~mask) && hit.collider.CompareTag("Sussy")) {
    //        OnSee?.Invoke();
    //        audioSource.volume = 1;
    //    }
    //    else {
    //        audioSource.volume = 0;
    //    }  
    //}

    private void FixedUpdate()
    {
        if (IsInView(player,sussy))
        {
            OnSee?.Invoke();
            audioSource.volume = 1;
        }
        else
        {
            audioSource.volume = 0;
        }
    }

    private bool IsInView(GameObject origin, GameObject toCheck)
    {
        Vector3 pointOnScreen = cam.WorldToScreenPoint(toCheck.GetComponentInChildren<Renderer>().bounds.center);

        //Is in front
        if (pointOnScreen.z < 0)
        {
            Debug.Log("Behind: " + toCheck.name);
            return false;
        }

        //Is in FOV
        if ((pointOnScreen.x < 0) || (pointOnScreen.x > Screen.width) ||
                (pointOnScreen.y < 0) || (pointOnScreen.y > Screen.height))
        {
            Debug.Log("OutOfBounds: " + toCheck.name);
            return false;
        }

        RaycastHit hit;
        Vector3 heading = toCheck.transform.position - origin.transform.position;
        Vector3 direction = heading.normalized;// / heading.magnitude;

        if (Physics.Linecast(cam.transform.position, toCheck.GetComponentInChildren<Renderer>().bounds.center, out hit))
        {
            if (hit.transform.name != toCheck.name)
            {
                Debug.DrawLine(cam.transform.position, toCheck.GetComponentInChildren<Renderer>().bounds.center, Color.red);
                Debug.LogError(toCheck.name + " occluded by " + hit.transform.name);

                Debug.Log(toCheck.name + " occluded by " + hit.transform.name);
                return false;
            }
        }
        return true;
    }


}
