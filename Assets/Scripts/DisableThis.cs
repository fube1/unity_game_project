using UnityEngine;

public class DisableThis : MonoBehaviour {
    void Start() {
        StartCoroutine(Utils.DelayedCall(TurnOff, 8.5f));
    }

    void TurnOff() {
        gameObject.SetActive(false);
    }
}
