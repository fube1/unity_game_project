using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interactor : MonoBehaviour
{
    [SerializeField]
    private char InteractKey = 'e';
    
    public Text InteractText;
    public FirstPersonLook FPSLook;
    public bool IsInteracting;

    private bool _isInBounds;
    private Interactable _interactable;

    public TaskManager TaskManager;

    private void Awake()
    {
        FPSLook = transform.Find("First person camera").GetComponent<FirstPersonLook>();
        TaskManager = GetComponent<TaskManager>();
    }

    private void Start()
    {
        SceneController.Instance.OnPause += (_, __) =>
        {
            FPSLook.enabled = false;
            InteractText.gameObject.SetActive(false);
        };

        SceneController.Instance.OnResume += (_, __) =>
        {
            FPSLook.enabled = true;
            InteractText.gameObject.SetActive(_isInBounds);
            
            if(IsInteracting)
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        };
    }

    private void FixedUpdate()
    {
            if (_interactable && !_interactable.IsInteractable)
            {
                InteractText.gameObject.SetActive(false);
                return;
            }

            if (IsInteracting)
            {
                InteractText.gameObject.SetActive(false);
            }

            if (!SceneController.Instance.GameIsPaused && _isInBounds && !IsInteracting && Input.GetKeyDown(InteractKey.ToString().ToLower()))
            {
                _interactable.OnInteract(this);
            }
        
    }

    void OnTriggerEnter(Collider interactionCollider)
    {
        _isInBounds = true;
        Interactable interactable = interactionCollider.GetComponent<Interactable>();
        if (interactable && interactable.IsInteractable)
        {
            _interactable = interactable;
            InteractText.gameObject.SetActive(true);
        }
    }

    void OnTriggerExit(Collider interactionCollider)
    {
        _isInBounds = false;
        Interactable interactable = interactionCollider.GetComponent<Interactable>();
        if (interactable)
        {
            InteractText.gameObject.SetActive(false);
        }
    }

}
