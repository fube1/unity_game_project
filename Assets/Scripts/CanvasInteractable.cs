using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CanvasInteractable : Interactable
{

    [SerializeField]
    protected GameObject OrthoCamera;
    [SerializeField]
    protected GameObject PerspectiveCamera;
    [SerializeField]
    protected GameObject _taskCanvas;

    protected bool IsInteracting;

    private void Update()
    {
        if (!SceneController.Instance.GameIsPaused && IsInteracting)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }

    private void Start()
    {
    }

    protected virtual void Engage(Interactor interactor)
    {
        IsInteracting = true;
        Debug.Log(interactor);
        if (interactor)
        {
            interactor.FPSLook.enabled = false;
            interactor.InteractText.gameObject.SetActive(false);
            interactor.IsInteracting = true;
        }

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        OrthoCamera.gameObject.SetActive(true);
        PerspectiveCamera.gameObject.SetActive(false);
    }

    protected virtual void Cleanup(Interactor interactor)
    {
        IsInteracting = false;
        if (interactor)
        {
            interactor.FPSLook.enabled = true;
            interactor.InteractText.gameObject.SetActive(true);
            interactor.IsInteracting = false;
            interactor.TaskManager.TaskCompleted(this);
        }

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        
        OrthoCamera.gameObject.SetActive(false);
        PerspectiveCamera.gameObject.SetActive(true);
        
        _taskCanvas.SetActive(false);
        enabled = false;
        IsInteractable = false;
        
    }
}
