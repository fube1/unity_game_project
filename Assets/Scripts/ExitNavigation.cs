using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitNavigation : MonoBehaviour
{
    public GameObject chaser;

    void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.name);
        chaser.SetActive(true);
    }
}
