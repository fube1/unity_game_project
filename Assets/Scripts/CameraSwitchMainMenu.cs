using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitchMainMenu : MonoBehaviour
{
    public GameObject cameraNavigation;
    public GameObject navigationLight;

    public GameObject cameraElectrical;
    public GameObject electricalLight;

    public GameObject cameraAsteroid;
    public GameObject asteroidLight;
    public GameObject cafeteriaLight;

    public GameObject cameraGas;
    public GameObject shieldLight;
    public GameObject radioLight;
    public GameObject gasLight;
    // Start is called before the first frame update
    void Awake()
    {
        StartCoroutine(Change());
    }

    IEnumerator Change()
    {
        while (true) {
            //Lights
            navigationLight.SetActive(true);
            electricalLight.SetActive(false);
            cafeteriaLight.SetActive(false);
            asteroidLight.SetActive(false);
            shieldLight.SetActive(false);
            radioLight.SetActive(false);
            gasLight.SetActive(false);
            //Cameras
            cameraNavigation.SetActive(true);
            cameraElectrical.SetActive(false);
            cameraAsteroid.SetActive(false);
            cameraGas.SetActive(false);
            yield return new WaitForSeconds(17.0f);

            //Lights
            navigationLight.SetActive(false);
            electricalLight.SetActive(true);
            cafeteriaLight.SetActive(false);
            asteroidLight.SetActive(false);
            shieldLight.SetActive(false);
            radioLight.SetActive(false);
            gasLight.SetActive(false);
            //Cameras
            cameraNavigation.SetActive(false);
            cameraElectrical.SetActive(true);
            cameraAsteroid.SetActive(false);
            cameraGas.SetActive(false);
            yield return new WaitForSeconds(13.0f);

            //Lights
            navigationLight.SetActive(false);
            electricalLight.SetActive(false);
            cafeteriaLight.SetActive(true);
            asteroidLight.SetActive(true);
            shieldLight.SetActive(false);
            radioLight.SetActive(false);
            gasLight.SetActive(false);
            //Cameras
            cameraNavigation.SetActive(false);
            cameraElectrical.SetActive(false);
            cameraAsteroid.SetActive(true);
            cameraGas.SetActive(false);
            yield return new WaitForSeconds(19.5f);

            //Lights
            navigationLight.SetActive(false);
            electricalLight.SetActive(false);
            cafeteriaLight.SetActive(false);
            asteroidLight.SetActive(false);
            shieldLight.SetActive(true);
            radioLight.SetActive(true);
            gasLight.SetActive(true);
            //Cameras
            cameraNavigation.SetActive(false);
            cameraElectrical.SetActive(false);
            cameraAsteroid.SetActive(false);
            cameraGas.SetActive(true);
            yield return new WaitForSeconds(18.5f);

        }

    }
}
