using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightsOnAndOff : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject chaser;
    private Light light;
    private float maxIntensity;
    private bool stop;
    void Start()
    {
        light = GetComponent<Light>();
        maxIntensity = light.intensity;
        stop = true;
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.name == chaser.name)
        {
            stop = false;
            StartCoroutine(Change());
            light.intensity = 0;
            //Debug.Log("Entered Collider Trigger: " + chaser.name);
        }
    }

    void OnTriggerExit(Collider collision)
    {
        if (collision.name == chaser.name)
        {
            stop = true;
            StopCoroutine(Change());
            light.intensity = maxIntensity;
            //Debug.Log("Exited Collider Trigger: " + chaser.name);
        }
    }

    IEnumerator Change()
    {
        while (true) {
            light.intensity = maxIntensity;
            if (stop == true)
                break;
            yield return new WaitForSeconds(0.25f);

            if (stop == true)
                break;
            light.intensity = 0;
            yield return new WaitForSeconds(0.25f);

            if (stop == true)
                break;
            light.intensity = maxIntensity;
            yield return new WaitForSeconds(1.25f);
        }


    }
}
