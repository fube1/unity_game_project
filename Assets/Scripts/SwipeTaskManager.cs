using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeTaskManager : MonoBehaviour, Completable
{
    private SwipeController _swipeController;
    private IndicatorController _indicatorController;

    public event EventHandler OnSuccess;
    public event EventHandler OnFail;

    [Header("Audio")]
    public AudioSource TaskAudio;
    public AudioClip StartAudio;
    public AudioClip SwipeAudio;
    public AudioClip SuccessAudio;
    public AudioClip FailAudio;

    void Awake()
    {
        TaskAudio.PlayOneShot(StartAudio);

        var cardInput = transform.Find("CardInput");

        _swipeController = cardInput.Find("SwipeArea").GetComponent<SwipeController>();
        _indicatorController = cardInput.Find("Indicators").GetComponent<IndicatorController>();

        _swipeController.OnSuccess += (_, __) =>
        {
            TaskAudio.PlayOneShot(SwipeAudio);
            TaskAudio.PlayOneShot(SuccessAudio);

            _indicatorController.LightSuccess();
            OnSuccess?.Invoke();
        };

        _swipeController.OnFail += (_, __) =>
        {
            TaskAudio.PlayOneShot(SwipeAudio);
            TaskAudio.PlayOneShot(FailAudio);

            _indicatorController.LightFail();
            OnFail?.Invoke();
        };
    }
}
