using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils
{
    public static IEnumerator DelayedCall(Action f1, float delay = 0.75f)
    {
        yield return new WaitForSeconds(delay);
        f1?.Invoke();
    }
}
