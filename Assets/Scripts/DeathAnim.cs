using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathAnim : MonoBehaviour {
    public AudioSource deathAudio;
    public AudioClip warning;
    public AudioClip boom;

    public AudioSource breathAudio;

    void CallOut() {
        deathAudio.PlayOneShot(warning);
        breathAudio.Stop();
    }

    void Boom() {
        deathAudio.PlayOneShot(boom);
    }

    void EndGame() {
        SceneManager.LoadScene("Lose");
    }
}
