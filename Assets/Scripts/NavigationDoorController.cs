using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationDoorController : MonoBehaviour
{
    [SerializeField]
    private TaskManager TaskManager;

    private void Start()
    {
        TaskManager.OnAllTasksComplete += (_, __) =>
        {
            gameObject.SetActive(false);
        };
    }
}
