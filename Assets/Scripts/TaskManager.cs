using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskManager : MonoBehaviour, Completable
{

    // Will be treated as a Set, meaning on Awake, it will remove dupes
    // This is because Unity can't serialize sets
    public List<Interactable> Tasks;
    private HashSet<Interactable> _tasks;

    public event EventHandler OnAllTasksComplete;
    public event EventHandler OnSuccess;
    public event EventHandler OnFail;

    private void Awake()
    {
        _tasks = new HashSet<Interactable>();
        Tasks.ForEach(n => _tasks.Add(n));
    }

    public void TaskCompleted(Interactable task)
    {
        Debug.Log(_tasks.Count);

        OnSuccess?.Invoke();
        _tasks.RemoveWhere(n => n.GetType().Name.Equals(task.GetType().Name)); // Only checking the type is not enough. It will remove everything that is derived from Interactable
        if(_tasks.Count == 0)
        {
            OnAllTasksComplete?.Invoke();
        }
    }
}
