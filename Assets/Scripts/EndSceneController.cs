using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndSceneController : MonoBehaviour
{
    private bool _canGoBack = false;
    private void Start()
    {
        Debug.Log("Started");
        _canGoBack = false;
        StartCoroutine(
            Utils.DelayedCall(() => 
            {
                _canGoBack = true;
            }, 8f)
        );
    }
    void FixedUpdate()
    {
        if(_canGoBack)
        {
            SceneManager.LoadScene("main_menu");
        }
    }
}
