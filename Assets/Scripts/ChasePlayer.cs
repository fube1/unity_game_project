using UnityEngine;
using UnityEngine.AI;

public class ChasePlayer : MonoBehaviour {

    public GameObject player;

    private NavMeshAgent agent;
    private Animator anim;
    private Interactor _interactor;
    private float _defSpeed;

    [Header("Audio")]
    public AudioSource stepAudio;

    void Start() 
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponentInChildren<Animator>();
        anim.SetBool("isRunning", true);
        _defSpeed = agent.speed;
        _interactor = player.GetComponent<Interactor>();
        player.GetComponent<RaycastController>().OnSee += (_, __) => agent.speed += 0.5f;
        player.GetComponent<TaskManager>().OnSuccess += (_, __) => agent.speed = _defSpeed;
    }

    void FixedUpdate() 
    {
        // Chase
        if (_interactor.IsInteracting)
        {
            agent.speed = 0;
            stepAudio.volume = 0;
        }
        else
        {
            //agent.speed = _defSpeed;
            stepAudio.volume = 0.3f;
        }
        agent.SetDestination(player.transform.position);

        // Audio
        if (!stepAudio.isPlaying)
            stepAudio.Play();
    }
}
