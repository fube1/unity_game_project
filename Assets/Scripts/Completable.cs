using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Completable
{
    event EventHandler OnSuccess;
    event EventHandler OnFail;
}
