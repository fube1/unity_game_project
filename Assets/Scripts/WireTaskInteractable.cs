using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WireTaskInteractable : CanvasInteractable
{
    public override void OnInteract(Interactor interactor)
    {
        base.Engage(interactor);
        var wgm = _taskCanvas.transform.GetComponentInChildren<WireGameManager>();

        wgm.OnFail += (_,__) => 
        {
            Debug.Log("Failed");
        };

        wgm.OnSuccess += (_, __) =>
        {
            Debug.Log("Success");
            Cleanup(interactor);
        };

        _taskCanvas.SetActive(true);
    }
}
