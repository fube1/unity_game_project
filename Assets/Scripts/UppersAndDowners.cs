using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UppersAndDowners : MonoBehaviour
{

    private bool _goDown;
    public float UpLimit = 15f;
    public float DownLimit = 10f;

    private void FixedUpdate()
    {
        Vector3 pos;
        if (_goDown)
        {
            pos = Vector3.down;
            _goDown = transform.position.y >= DownLimit;
        }
        else
        {
            pos = Vector3.up;
            _goDown = transform.position.y >= UpLimit;
        }
        pos *= 3;
        transform.Translate(pos * Time.deltaTime, Space.World);
    }
}
