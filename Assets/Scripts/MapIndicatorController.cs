using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapIndicatorController : MonoBehaviour
{
    [SerializeField]
    private GameObject CompletableObject;
    private Completable RelatedCompletable;
    void Start()
    {
        RelatedCompletable = CompletableObject.GetComponent<Completable>();
        if(RelatedCompletable != null)
        {
            RelatedCompletable.OnSuccess += (_, __) => Destroy(gameObject);
        }
    }
}
