using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardNumberTaskInteractable : CanvasInteractable
{
    public override void OnInteract(Interactor interactor)
    {

        Engage(interactor);
        var cgm = _taskCanvas.transform.GetComponent<CardGameManager>();

        cgm.OnFail += (_, __) =>
        {
            Debug.Log("Failed");
        };

        cgm.OnSuccess += (_, __) =>
        {
            Debug.Log("Success");
            StartCoroutine(Utils.DelayedCall(() => Cleanup(interactor)));
        };

        _taskCanvas.SetActive(true);
    }
}
