﻿using System.Collections.Generic;
using UnityEngine;

public class FirstPersonMovement : MonoBehaviour {
    [Header("Speed")]
    public float speed = 5f;
    public float gravity = -9.81f;
    Vector3 velocity;

    [Header("Audio")]
    public AudioSource stepAudio;
    public AudioSource runningAudio;

    [Header("Running")]
    public bool canRun = true;
    public bool IsRunning { get; private set; }
    public object SceneLoader { get; private set; }

    public float runSpeed = 9f;
    public KeyCode runningKey = KeyCode.LeftShift;
    /// <summary> Functions to override movement speed. Will use the last added override. </summary>
    public List<System.Func<float>> speedOverrides = new List<System.Func<float>>();

    // Custom Animator
    private CharacterController controller;
    private Animator anim;

    // DeathRoom
    [Header("Death Room")]
    public GameObject deathRoom;
    public GameObject sussy;

    void Start() {
        controller = GetComponent<CharacterController>();
        anim = GetComponentInChildren<Animator>();
    }

    void FixedUpdate() {
        // Move
        IsRunning = canRun && Input.GetKey(runningKey);
        float movingSpeed = IsRunning ? runSpeed : speed;
        if (speedOverrides.Count > 0)
            movingSpeed = speedOverrides[speedOverrides.Count - 1]();

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        controller.Move(move * movingSpeed * Time.deltaTime);

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);

        // Animation
        if (x == 0 && z == 0) {
            anim.SetBool("isIdle", true);
            anim.SetBool("isRunning", false);
            // Audio
            stepAudio.Stop();
            runningAudio.Stop();
        }
        else {
            anim.SetBool("isRunning", true);
            anim.SetBool("isIdle", false);
            // Audio
            if (IsRunning) {
                stepAudio.Stop();
                if (!runningAudio.isPlaying)
                    runningAudio.Play();
            }
            else {
                runningAudio.Stop();
                if (!stepAudio.isPlaying)
                    stepAudio.Play();
            }
        }
    }

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("Sussy")) {
            gameObject.SetActive(false);
            sussy.SetActive(false);
            deathRoom.SetActive(true);
        }
    }
}